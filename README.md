## Media Convert

These scripts will convert one format to another pretty quickly.

I symlink em' to `/usr/local/bin` so I can run them from anywhere.

Requres `ffmpeg`.

#### flac-mp3

Converts all flac files in the current directory to mp3 using
VBR V0 settings. 

### m4a-mp3

Converts all m4a files to mp3 using VBR V0 settings. 

### shn-flac

Converts all shn files in the current directory to flac. shn is a form of
lossless compression so going from shn > flac is probably ok.

### shn-mp3

Converts all shn files in the current directory to mp3 using 
VBR V0 settings.

### wav-flac

Converts all wav files in the current directory to flac.

### wav-mp3

Converts all wav files in the current directory to mp3 using 
VBR V0 settings.

### webm-mp3

Converts all webm files in the current directory to mp3 using 
VBR V0 settings.

----

### General Info

**Uncompressed Lossless Formats**
- WAV
- AIFF


**Compressed Lossless Formats**
- FLAC
- ALAC
- APE
- SHN


**Lossy Formats**
- MP3
- AAC
- WMA
- AC3
etc..

#### Quality

- **Best** = Uncompressed Lossless > Other Format.
- **Better** = Compressed Lossless > Other Format.
- **Bad** = Lossy > Lossy

You should _never_ convert Lossy > Compressed Lossless. It will not
make a true Lossless file.

Converting from Lossy > Lossy will degrade the quality even more.

[FFMPEG INFO](https://trac.ffmpeg.org/wiki/Encode/MP3)
